clang-format src/*.c -i
clang-format src/*.h -i
clang-format src/*.cpp -i
clang-format shaders/*.frag -i
clang-format shaders/*.vert -i
