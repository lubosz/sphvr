# SPHVR

A spherical media player using Vulkan for rendering.

![SPHVR Logo](res/icon.svg)

## Dependencies

* gulkan
* gxr
* glfw
* glm
* gstreamer-vulkan

## Build

```
meson build
ninja -C build
```

## Usage

```
Usage:
  sphvr [OPTION?] [FILE] - Render spherical media.

Examples:

View a equirect image
sphvr foo.jpg

Help Options:
  -h, --help           Show help options

Application Options:
  -s, --stereo         Stereo mode: none (default), side-by-side, top-bottom
  -r, --phi-range      Phi range: 360 (default), 180
  -g, --gamma          Gamma correction: 1 (default)
  -i, --image          Media is a still image. Default: not set
  -p, --projection     Projection type. One of: equirect (default), cube, eac
  -o, --output         Output type. One of: desktop (default), xr
```

## Desktop Mode Keys

```
Left mouse : Drag
Mouse Wheel: Zoom
F          : Toggle full screen
Space      : Toggle pause
Left/Right : Seek 10s
ESC        : Quit
```

## Examples

### Play 360 equirect video

```
sphvr file:///full/path/to/video.webm
```

### View 360 equirect image in XR

```
sphvr -i res/equirect.jpg -o xr
```

### Play side-by-side stereo 180 equirect video

```
sphvr -r 180 -s side-by-side file:///full/path/to/video.mp4
```

### Play 360 equi-angular-cubemap video

```
sphvr -p eac file:///full/path/to/video.mp4
```
