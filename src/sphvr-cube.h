/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

// clang-format off
const float positions[] = {
  // front
  -1.0f, -1.0f, +1.0f, // point blue
  +1.0f, -1.0f, +1.0f, // point magenta
  -1.0f, +1.0f, +1.0f, // point cyan
  +1.0f, +1.0f, +1.0f, // point white
  // back
  +1.0f, -1.0f, -1.0f, // point red
  -1.0f, -1.0f, -1.0f, // point black
  +1.0f, +1.0f, -1.0f, // point yellow
  -1.0f, +1.0f, -1.0f, // point green
  // right
  +1.0f, -1.0f, +1.0f, // point magenta
  +1.0f, -1.0f, -1.0f, // point red
  +1.0f, +1.0f, +1.0f, // point white
  +1.0f, +1.0f, -1.0f, // point yellow
  // left
  -1.0f, -1.0f, -1.0f, // point black
  -1.0f, -1.0f, +1.0f, // point blue
  -1.0f, +1.0f, -1.0f, // point green
  -1.0f, +1.0f, +1.0f, // point cyan
  // top
  -1.0f, +1.0f, +1.0f, // point cyan
  +1.0f, +1.0f, +1.0f, // point white
  -1.0f, +1.0f, -1.0f, // point green
  +1.0f, +1.0f, -1.0f, // point yellow
  // bottom
  -1.0f, -1.0f, -1.0f, // point black
  +1.0f, -1.0f, -1.0f, // point red
  -1.0f, -1.0f, +1.0f, // point blue
  +1.0f, -1.0f, +1.0f  // point magenta
};

const float uvs[] = {
  // Front face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Back face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Top face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Bottom face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Right face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Left face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,
};

const float uvs_eac[] = {
  // Front face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Back face
  1.0, 1.0,
  1.0, 0.0,
  0.0, 1.0,
  0.0, 0.0,

  // Left face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Right face
  1.0, 0.0,
  0.0, 0.0,
  1.0, 1.0,
  0.0, 1.0,

  // Bottom face
  0.0, 0.0,
  0.0, 1.0,
  1.0, 0.0,
  1.0, 1.0,

  // Top face
  0.0, 0.0,
  0.0, 1.0,
  1.0, 0.0,
  1.0, 1.0,
};

static const float offsets[] = {
  // Front face
  1. / 3., 1. / 2.,
  1. / 3., 1. / 2.,
  1. / 3., 1. / 2.,
  1. / 3., 1. / 2.,

  // Back face
  2. / 3., 1. / 2.,
  2. / 3., 1. / 2.,
  2. / 3., 1. / 2.,
  2. / 3., 1. / 2.,

  // Top face
  1. / 3., 0,
  1. / 3., 0,
  1. / 3., 0,
  1. / 3., 0,

  // Bottom face
  0, 0,
  0, 0,
  0, 0,
  0, 0,

  // Right face
  0., 1. / 2.,
  0., 1. / 2.,
  0., 1. / 2.,
  0., 1. / 2.,

  // Left face
  2. / 3., 0,
  2. / 3., 0,
  2. / 3., 0,
  2. / 3., 0,
};

const float EPSILON = 0.0012f;

static const float offsets_eac[] = {
  // Front face
  1. / 3., EPSILON,
  1. / 3., EPSILON,
  1. / 3., EPSILON,
  1. / 3., EPSILON,

  // Back face
  1. / 3., 1. / 2. + EPSILON,
  1. / 3., 1. / 2. + EPSILON,
  1. / 3., 1. / 2. + EPSILON,
  1. / 3., 1. / 2. + EPSILON,

  // Top face
  0, EPSILON,
  0, EPSILON,
  0, EPSILON,
  0, EPSILON,

  // Bottom face
  2. / 3., EPSILON,
  2. / 3., EPSILON,
  2. / 3., EPSILON,
  2. / 3., EPSILON,

  // Right face
  0., 1. / 2. + EPSILON,
  0., 1. / 2. + EPSILON,
  0., 1. / 2. + EPSILON,
  0., 1. / 2. + EPSILON,

  // Left face
  2. / 3., 1. / 2. + EPSILON,
  2. / 3., 1. / 2. + EPSILON,
  2. / 3., 1. / 2. + EPSILON,
  2. / 3., 1. / 2. + EPSILON,
};
// clang-format on
