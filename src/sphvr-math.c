#include "sphvr-math.h"

#include <glib.h>

void
sphvr_math_inverse_view_projection (const graphene_matrix_t *view,
                                    const graphene_matrix_t *projection,
                                    graphene_matrix_t *result)
{
  float m[4][4];
  graphene_matrix_to_float (view, *m);
  m[0][3] = 0;
  m[1][3] = 0;
  m[2][3] = 0;
  m[3][0] = 0;
  m[3][1] = 0;
  m[3][2] = 0;
  m[3][3] = 1.0f;

  graphene_matrix_t view3x3;
  graphene_matrix_init_from_float (&view3x3, *m);

  graphene_matrix_t vp;
  graphene_matrix_multiply (&view3x3, projection, &vp);

  graphene_matrix_inverse (&vp, result);
}

void
sphvr_math_fix_handedness (const graphene_matrix_t *in, graphene_matrix_t *out)
{
  float m[4][4];
  graphene_matrix_to_float (in, *m);
  m[0][1] = -m[0][1];
  m[1][0] = -m[1][0];
  m[1][2] = -m[1][2];
  m[2][1] = -m[2][1];
  graphene_matrix_init_from_float (out, *m);
}
