/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-app.h"

#include "sphvr-camera-desktop.h"
#include "sphvr-enums.h"
#include "sphvr-image.h"
#include "sphvr-pipeline.h"
#include "sphvr-video.h"

struct _SphvrAppDesktop
{
  SphvrApp parent;

  SphvrCameraDesktop *camera;

  gchar *path;

  GulkanWindow *window;
};

G_DEFINE_TYPE (SphvrAppDesktop, sphvr_app_desktop, SPHVR_TYPE_APP)

static void
sphvr_app_desktop_init (SphvrAppDesktop *self)
{
  self->camera = sphvr_camera_desktop_new ();
  self->window = NULL;
  self->path = NULL;
}

static void
_finalize (GObject *gobject)
{
  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (gobject);
  if (self->path) {
    g_free (self->path);
  }
  g_object_unref (self->camera);
  G_OBJECT_CLASS (sphvr_app_desktop_parent_class)->finalize (gobject);

  // Wayland surface needs to be deinited after vk swapchain
  g_object_unref (self->window);
}

static gboolean
_iterate (SphvrApp *app)
{
  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (app);

  gulkan_window_poll_events (self->window);

  SphvrRenderer *renderer = sphvr_app_get_renderer (SPHVR_APP (self));

  sphvr_renderer_draw (renderer);

  return TRUE;
}

static void
_toggle_pause (SphvrAppDesktop *self)
{
  if (sphvr_app_is_still_image (SPHVR_APP (self)))
    return;

  SphvrVideo *video = sphvr_app_get_video (SPHVR_APP (self));
  sphvr_video_toggle_pause (video);
}

static void
_seek (SphvrAppDesktop *self, int seconds)
{
  if (sphvr_app_is_still_image (SPHVR_APP (self)))
    return;

  SphvrVideo *video = sphvr_app_get_video (SPHVR_APP (self));
  sphvr_video_seek (video, seconds);
}

static void
_key_cb (GulkanWindow *window, GulkanKeyEvent *event, gpointer _self)
{
  // We are only interested in press events.
  if (!event->is_pressed)
    return;

  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (_self);
  switch (event->key) {
    case XKB_KEY_Escape:
      sphvr_app_quit (SPHVR_APP (self));
      break;
    case XKB_KEY_f:
      gulkan_window_toggle_fullscreen (window);
      break;
    case XKB_KEY_space:
      _toggle_pause (self);
      break;
    case XKB_KEY_Left:
      _seek (self, -10);
      break;
    case XKB_KEY_Right:
      _seek (self, 10);
      break;
    default:
      break;
  }
}

static void
_configure_cb (GulkanWindow *window, GulkanConfigureEvent *event,
               gpointer _self)
{
  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (_self);

  SphvrRenderer *sphvr_renderer = sphvr_app_get_renderer (SPHVR_APP (self));
  GulkanRenderer *gulkan_renderer = sphvr_renderer_get_renderer (
    sphvr_renderer);
  GulkanContext *gulkan = gulkan_renderer_get_context (gulkan_renderer);

  VkSurfaceKHR surface;
  VkInstance instance = gulkan_context_get_instance_handle (gulkan);
  if (gulkan_window_create_surface (window, instance, &surface)
      != VK_SUCCESS) {
    g_printerr ("Creating surface failed.");
    return;
  }

  GulkanSwapchainRenderer *scr = GULKAN_SWAPCHAIN_RENDERER (gulkan_renderer);

  if (!gulkan_swapchain_renderer_resize (scr, surface, event->extent))
    g_warning ("Resize failed.");

  self->camera->aspect = (float) event->extent.width
                         / (float) event->extent.height;
  sphvr_camera_desktop_update_view (self->camera);

  SphvrPipeline *pipeline = sphvr_renderer_get_pipeline (sphvr_renderer);

  sphvr_pipeline_set_vp (pipeline, &self->camera->mvp);
}

static void
_pointer_position_cb (GulkanWindow *window, GulkanPositionEvent *event,
                      gpointer _self)
{
  (void) window;

  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (_self);

  if (self->camera->inverse_mode)
    sphvr_camera_desktop_set_cursor_position (self->camera, -event->offset.x,
                                              event->offset.y);
  else
    sphvr_camera_desktop_set_cursor_position (self->camera, -event->offset.x,
                                              -event->offset.y);

  SphvrRenderer *renderer = sphvr_app_get_renderer (SPHVR_APP (self));

  SphvrPipeline *pipeline = sphvr_renderer_get_pipeline (renderer);

  sphvr_pipeline_set_vp (pipeline, &self->camera->mvp);
}

static void
_pointer_button_cb (GulkanWindow *window, GulkanButtonEvent *event,
                    gpointer _self)
{
  (void) window;

  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (_self);
  if (event->button == BTN_LEFT) {
    self->camera->mouse_pressed = event->is_pressed;
  }
}

static void
_pointer_axis_cb (GulkanWindow *window, GulkanAxisEvent *event, gpointer _self)
{
  (void) window;

  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (_self);
  if (event->value > 0)
    self->camera->fov /= self->camera->zoom_step;
  else
    self->camera->fov *= self->camera->zoom_step;

  if (self->camera->fov < self->camera->min_fov)
    self->camera->fov = self->camera->min_fov;

  if (self->camera->fov > self->camera->max_fov)
    self->camera->fov = self->camera->max_fov;

  sphvr_camera_desktop_update_view (self->camera);

  SphvrRenderer *renderer = sphvr_app_get_renderer (SPHVR_APP (self));

  SphvrPipeline *pipeline = sphvr_renderer_get_pipeline (renderer);

  sphvr_pipeline_set_vp (pipeline, &self->camera->mvp);
}

static void
_close_cb (SphvrRendererDesktop *renderer, gpointer _self)
{
  (void) renderer;

  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (_self);
  sphvr_app_quit (SPHVR_APP (self));
}

static void
_pipeline_init_cb (GulkanWindow *window, gpointer _self)
{
  (void) window;

  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (_self);
  if (!sphvr_app_init_path (SPHVR_APP (self), self->path)) {
    g_printerr ("Could not init media\n");
  }
}

static gboolean
_inititalize (SphvrApp *app, const gchar *path)
{
  SphvrAppDesktop *self = SPHVR_APP_DESKTOP (app);

  self->path = g_strdup (path);

  SphvrRenderer *renderer = sphvr_app_get_renderer (SPHVR_APP (self));

  SphvrRendererDesktop *renderer_desktop = SPHVR_RENDERER_DESKTOP (renderer);
  g_signal_connect (renderer_desktop, "pipeline-init",
                    (GCallback) _pipeline_init_cb, self);

  VkExtent2D extent = {1280, 720};

  gulkan_renderer_set_extent (sphvr_renderer_get_renderer (renderer), extent);

  self->window = gulkan_window_new (extent, "SPHVR");
  if (!self->window) {
    g_printerr ("Could not initialize window.\n");
    return FALSE;
  }

  GSList *instance_ext_list = gulkan_window_required_extensions (self->window);

  const gchar *device_extensions[] = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    VK_KHR_MULTIVIEW_EXTENSION_NAME,
  };

  GSList *device_ext_list = NULL;
  for (uint64_t i = 0; i < G_N_ELEMENTS (device_extensions); i++) {
    char *device_ext = g_strdup (device_extensions[i]);
    device_ext_list = g_slist_append (device_ext_list, device_ext);
  }

  GulkanContext *gulkan
    = gulkan_context_new_from_extensions (instance_ext_list, device_ext_list,
                                          VK_NULL_HANDLE);

  if (!gulkan_window_has_support (self->window, gulkan)) {
    g_printerr ("Window surface extension support check failed.\n");
    return FALSE;
  }

  g_slist_free (instance_ext_list);
  g_slist_free (device_ext_list);

  gulkan_renderer_set_context (sphvr_renderer_get_renderer (renderer), gulkan);

  g_signal_connect (self->window, "configure", (GCallback) _configure_cb,
                    self);
  g_signal_connect (self->window, "pointer-position",
                    (GCallback) _pointer_position_cb, self);
  g_signal_connect (self->window, "close", (GCallback) _close_cb, self);
  g_signal_connect (self->window, "key", (GCallback) _key_cb, self);

  g_signal_connect (self->window, "pointer-axis", (GCallback) _pointer_axis_cb,
                    self);
  g_signal_connect (self->window, "pointer-button",
                    (GCallback) _pointer_button_cb, self);

  g_object_unref (gulkan);

  sphvr_renderer_initialize (renderer);

  SphvrPipeline *p = sphvr_renderer_get_pipeline (renderer);
  sphvr_projection_type t = sphvr_pipeline_get_projection_type (p);
  self->camera->inverse_mode = t == SPHVR_PROJECTION_EQUIRECT;

  return TRUE;
}

static void
sphvr_app_desktop_class_init (SphvrAppDesktopClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;

  SphvrAppClass *sphvr_app_class = SPHVR_APP_CLASS (klass);
  sphvr_app_class->iterate = _iterate;
  sphvr_app_class->initialize = _inititalize;
}
