#pragma once

#include <graphene.h>

void
sphvr_math_inverse_view_projection (const graphene_matrix_t *view,
                                    const graphene_matrix_t *projection,
                                    graphene_matrix_t *result);

void
sphvr_math_fix_handedness (const graphene_matrix_t *in,
                           graphene_matrix_t *out);
