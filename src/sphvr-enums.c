/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-enums.h"

#define ENUM_TO_STR(r)                                                        \
  case r:                                                                     \
    return #r

const char *
sphvr_stereo_mode_str (sphvr_stereo_mode code)
{
  switch (code) {
    ENUM_TO_STR (SPHVR_STEREO_NONE);
    ENUM_TO_STR (SPHVR_STEREO_SBS);
    ENUM_TO_STR (SPHVR_STEREO_TB);
    default:
      return "UNKNOWN";
  }
}

const char *
sphvr_φ_range_str (sphvr_φ_range code)
{
  switch (code) {
    ENUM_TO_STR (SPHVR_φ_360);
    ENUM_TO_STR (SPHVR_φ_180);
    default:
      return "UNKNOWN";
  }
}

const char *
sphvr_projection_type_str (sphvr_projection_type code)
{
  switch (code) {
    ENUM_TO_STR (SPHVR_PROJECTION_EQUIRECT);
    ENUM_TO_STR (SPHVR_PROJECTION_CUBE);
    ENUM_TO_STR (SPHVR_PROJECTION_EAC);
    default:
      return "UNKNOWN";
  }
}

const char *
sphvr_output_type_str (sphvr_output_type code)
{
  switch (code) {
    ENUM_TO_STR (SPHVR_OUTPUT_XR);
    ENUM_TO_STR (SPHVR_OUTPUT_DESKTOP);
    default:
      return "UNKNOWN";
  }
}
