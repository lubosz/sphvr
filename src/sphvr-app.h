/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <glib-object.h>

#include "sphvr-enums.h"

#define SPHVR_TYPE_APP sphvr_app_get_type ()
G_DECLARE_DERIVABLE_TYPE (SphvrApp, sphvr_app, SPHVR, APP, GObject)

struct _SphvrAppClass
{
  GObjectClass parent;

  gboolean (*iterate) (SphvrApp *self);

  gboolean (*initialize) (SphvrApp *self, const gchar *path);
};

#define SPHVR_TYPE_APP_DESKTOP sphvr_app_desktop_get_type ()
G_DECLARE_FINAL_TYPE (SphvrAppDesktop, sphvr_app_desktop, SPHVR, APP_DESKTOP,
                      SphvrApp)

#define SPHVR_TYPE_APP_XR sphvr_app_xr_get_type ()
G_DECLARE_FINAL_TYPE (SphvrAppXr, sphvr_app_xr, SPHVR, APP_XR, SphvrApp)

typedef struct _SphvrRenderer SphvrRenderer;
typedef struct _SphvrVideo SphvrVideo;

SphvrApp *
sphvr_app_new (sphvr_output_type output_type, float gamma,
               sphvr_stereo_mode stereo_mode,
               sphvr_projection_type projection_type, sphvr_φ_range φ_range,
               gboolean is_still_image);

void
sphvr_app_run (SphvrApp *self);

void
sphvr_app_quit (SphvrApp *self);

SphvrRenderer *
sphvr_app_get_renderer (SphvrApp *self);

SphvrVideo *
sphvr_app_get_video (SphvrApp *self);

gboolean
sphvr_app_init_path (SphvrApp *self, const gchar *path);

gboolean
sphvr_app_is_still_image (SphvrApp *self);

gboolean
sphvr_app_initialize (SphvrApp *self, const gchar *path);
