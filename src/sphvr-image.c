/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-image.h"

struct _SphvrImage
{
  GObject parent;

  GdkPixbuf *pixbuf;
  GulkanTexture *texture;
};

G_DEFINE_TYPE (SphvrImage, sphvr_image, G_TYPE_OBJECT)

static void
sphvr_image_init (SphvrImage *self)
{
  (void) self;
}

SphvrImage *
sphvr_image_new (void)
{
  return (SphvrImage *) g_object_new (SPHVR_TYPE_IMAGE, 0);
}

static void
_finalize (GObject *gobject)
{
  SphvrImage *self = SPHVR_IMAGE (gobject);
  g_clear_object (&self->texture);
  g_object_unref (self->pixbuf);
  G_OBJECT_CLASS (sphvr_image_parent_class)->finalize (gobject);
}

static void
sphvr_image_class_init (SphvrImageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

static GdkPixbuf *
_load_pixbuf_from_file (const gchar *path)
{
  GError *error = NULL;
  GdkPixbuf *pixbuf_no_alpha = gdk_pixbuf_new_from_file (path, &error);

  if (error != NULL) {
    g_printerr ("Unable to read file: %s\n", error->message);
    g_error_free (error);
    return NULL;
  } else {
    GdkPixbuf *pixbuf = gdk_pixbuf_add_alpha (pixbuf_no_alpha, FALSE, 0, 0, 0);
    g_object_unref (pixbuf_no_alpha);
    return pixbuf;
  }
}

static GdkPixbuf *
_load_pixbuf_from_res (const gchar *path)
{
  GError *error = NULL;
  GdkPixbuf *pixbuf_no_alpha = gdk_pixbuf_new_from_resource (path, &error);

  if (error != NULL) {
    g_printerr ("Unable to read file: %s\n", error->message);
    g_error_free (error);
    return NULL;
  } else {
    GdkPixbuf *pixbuf = gdk_pixbuf_add_alpha (pixbuf_no_alpha, FALSE, 0, 0, 0);
    g_object_unref (pixbuf_no_alpha);
    return pixbuf;
  }
}

bool
sphvr_image_load_from_path (SphvrImage *self, const char *path)
{
  self->pixbuf = _load_pixbuf_from_file (path);
  if (self->pixbuf == NULL)
    return false;

  return true;
}

bool
sphvr_image_load_from_res (SphvrImage *self, const char *path)
{
  self->pixbuf = _load_pixbuf_from_res (path);
  if (self->pixbuf == NULL)
    return false;

  return true;
}

bool
sphvr_image_init_texture (SphvrImage *self, GulkanContext *client)
{
  self->texture
    = gulkan_texture_new_from_pixbuf (client, self->pixbuf,
                                      VK_FORMAT_R8G8B8A8_SRGB,
                                      VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                      TRUE);
  if (!self->texture)
    return false;

  if (!gulkan_texture_init_sampler (self->texture, VK_FILTER_LINEAR,
                                    VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE))
    return false;

  return true;
}

VkSampler
sphvr_image_get_sampler (SphvrImage *self)
{
  return gulkan_texture_get_sampler (self->texture);
}

VkImageView
sphvr_image_get_view (SphvrImage *self)
{
  return gulkan_texture_get_image_view (self->texture);
}
