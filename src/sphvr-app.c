/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib-unix.h>

#include "sphvr-app.h"

#include "sphvr-app.h"
#include "sphvr-image.h"
#include "sphvr-pipeline.h"
#include "sphvr-renderer.h"
#include "sphvr-video.h"

typedef struct _SphvrAppPrivate
{
  GObject parent;

  GMainLoop *loop;

  SphvrVideo *video;
  SphvrImage *image;

  gboolean is_still_image;

  SphvrRenderer *renderer;

  guint sigint_signal;
} SphvrAppPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (SphvrApp, sphvr_app, G_TYPE_OBJECT)

static gboolean
_sigint_cb (gpointer _self)
{
  SphvrApp *self = (SphvrApp *) _self;
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  g_main_loop_quit (priv->loop);
  return TRUE;
}

static void
sphvr_app_init (SphvrApp *self)
{
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  priv->loop = g_main_loop_new (NULL, FALSE);
  priv->is_still_image = FALSE;
  priv->sigint_signal = g_unix_signal_add (SIGINT, _sigint_cb, self);
}

static void
_finalize (GObject *gobject)
{
  SphvrApp *self = SPHVR_APP (gobject);
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);

  g_source_remove (priv->sigint_signal);

  g_main_loop_unref (priv->loop);
  g_clear_object (&priv->image);
  g_clear_object (&priv->video);
  g_object_unref (priv->renderer);

  G_OBJECT_CLASS (sphvr_app_parent_class)->finalize (gobject);
}

static void
sphvr_app_class_init (SphvrAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

SphvrApp *
sphvr_app_new (sphvr_output_type output_type, float gamma,
               sphvr_stereo_mode stereo_mode,
               sphvr_projection_type projection_type, sphvr_φ_range φ_range,
               gboolean is_still_image)
{
  GType app_type;
  GType renderer_type;

  switch (output_type) {
    case SPHVR_OUTPUT_DESKTOP:
      app_type = SPHVR_TYPE_APP_DESKTOP;
      renderer_type = SPHVR_TYPE_RENDERER_DESKTOP;
      break;
    case SPHVR_OUTPUT_XR:
      app_type = SPHVR_TYPE_APP_XR;
      renderer_type = SPHVR_TYPE_RENDERER_XR;
      break;
    default:
      g_printerr ("Unknown output type %d.\n", output_type);
      g_assert ("UNREACHABLE" && FALSE);
      return NULL;
  }

  SphvrApp *self = SPHVR_APP (g_object_new (app_type, 0));
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  priv->is_still_image = is_still_image;
  priv->renderer = SPHVR_RENDERER (g_object_new (renderer_type, 0));

  SphvrPipeline *pipeline = sphvr_pipeline_new (projection_type, gamma,
                                                stereo_mode, φ_range);

  sphvr_renderer_set_pipeline (priv->renderer, pipeline);

  return self;
}

void
sphvr_app_quit (SphvrApp *self)
{
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  g_main_loop_quit (priv->loop);
}

static gboolean
_draw_cb (gpointer data)
{
  SphvrApp *self = (SphvrApp *) data;
  SphvrAppClass *klass = SPHVR_APP_GET_CLASS (self);
  if (klass->iterate == NULL)
    return FALSE;

  return klass->iterate (self);
}

void
sphvr_app_run (SphvrApp *self)
{
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  g_timeout_add (1, _draw_cb, self);
  g_main_loop_run (priv->loop);
}

SphvrRenderer *
sphvr_app_get_renderer (SphvrApp *self)
{
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  return priv->renderer;
}

SphvrVideo *
sphvr_app_get_video (SphvrApp *self)
{
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  return priv->video;
}

gboolean
sphvr_app_init_path (SphvrApp *self, const gchar *path)
{
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);

  /* TODO: Detect image or video */
  if (priv->is_still_image) {
    priv->image = sphvr_image_new ();

    if (!sphvr_image_load_from_path (priv->image, path))
      return FALSE;

    GulkanContext *gulkan = gulkan_renderer_get_context (
      GULKAN_RENDERER (priv->renderer));

    if (!sphvr_image_init_texture (priv->image, gulkan))
      return FALSE;

    VkImageView vk_image_view = sphvr_image_get_view (priv->image);
    VkSampler sampler = sphvr_image_get_sampler (priv->image);

    SphvrPipeline *pipeline = sphvr_renderer_get_pipeline (priv->renderer);

    sphvr_pipeline_set_image_view (pipeline, vk_image_view, sampler);
  } else {
    priv->video = sphvr_video_new ();
    sphvr_video_set_renderer (priv->video, priv->renderer);
    if (!sphvr_video_initialize (priv->video, path)) {
      printf ("Could not create gst pipeline!\n");
    }
  }

  return TRUE;
}

gboolean
sphvr_app_is_still_image (SphvrApp *self)
{
  SphvrAppPrivate *priv = sphvr_app_get_instance_private (self);
  return priv->is_still_image;
}

gboolean
sphvr_app_initialize (SphvrApp *self, const gchar *path)
{
  SphvrAppClass *klass = SPHVR_APP_GET_CLASS (self);
  if (klass->iterate == NULL)
    return FALSE;

  return klass->initialize (self, path);
}
