/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#version 460 core

layout (location = 0) in vec2 in_uv;
layout (location = 1) in vec2 in_offset;

#define INV_PI_2 0.636619772f

layout (binding = 0) uniform Transformation
{
  mat4 mvp[2];
  float gamma;
  float unused;
  uint stereo_mode;
  uint phi_range;
}
ubo;

layout (binding = 1) uniform sampler2D image;

layout (location = 0) out vec4 out_color;

const vec2 scale = vec2 (1.0f / 3.0f, 1.0f / 2.0f);

void
main ()
{
  vec2 uv = (in_uv * scale) + in_offset;
  vec2 epsilon = vec2 (0.0005);
  uv = clamp (uv, in_offset + epsilon, in_offset + scale - epsilon);
  out_color = texture (image, uv);
}
