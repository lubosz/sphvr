/*
 * sphvr
 * Copyright 2018 Dongseong Hwang <dongseong.hwang@intel.com>
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: BSD-3-Clause
 */

#version 460 core

layout (location = 0) in vec2 in_uv;
layout (location = 1) in vec2 in_offset;

#define INV_PI_2 0.636619772f
#define EPSILON 0.0012f

layout (binding = 0) uniform Transformation
{
  mat4 mvp[2];
  float gamma;
  float unused;
  uint stereo_mode;
  uint phi_range;
}
ubo;

layout (binding = 1) uniform sampler2D image;

layout (location = 0) out vec4 out_color;

const vec2 scale = vec2 (1.0f / 3.0f, 1.0f / 2.0f - 2 * EPSILON);

void
main ()
{
  vec2 homogeneouseUv = (in_uv * 2.) - 1.;
  // Get UV on the EAC projection
  vec2 eacUv = (INV_PI_2 * atan (homogeneouseUv)) + 0.5;
  vec2 uvFor6Faces = (eacUv * scale) + in_offset;

  out_color = texture (image, uvFor6Faces);
}
