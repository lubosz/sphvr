/*
 * sphvr
 * Copyright 2016-2020 Collabora Ltd.
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#version 460

#extension GL_EXT_multiview : enable

layout (location = 0) in vec2 uv;

layout (binding = 0) uniform Transformation
{
  mat4 mvp[2];
  float gamma;
  float unused;
  uint stereo_mode;
  uint phi_range;
}
ubo;

layout (binding = 1) uniform sampler2D image;

layout (location = 0) out vec4 out_color;

const float PI = 3.1416;

//#define DEBUG 1

void
main ()
{
  vec2 frag_coord = vec2 (uv) * 2 - 1;

  vec4 view_dir = normalize (ubo.mvp[gl_ViewIndex]
                             * vec4 (frag_coord.x, -frag_coord.y, 1, 1));

  float lat = atan (view_dir.x, -view_dir.z) / (2 * PI);

  lat += 0.5f;

  float lon = acos (view_dir.y) / PI;

#ifdef DEBUG
  int lat_int = int (lat * 1000.0);
  int lon_int = int (lon * 1000.0);

  if (lat < 0.001 && lat > -0.001)
    out_color = vec4 (1, 0, 0, 1);
  else if (lat_int == 499)
    out_color = vec4 (0, 0, 1, 1);
  else if (lat_int == 999)
    out_color = vec4 (0, 1, 0, 1);
  else if (lat_int % 50 == 0)
    out_color = vec4 (1, 1, 1, 1);
  else if (lon_int % 50 == 0)
    out_color = vec4 (1, 1, 1, 1);
  else
    out_color = vec4 (lat, lon, 0, 1);
#endif

  vec2 sample_uv;
  switch (ubo.stereo_mode) {
    case 1:
      /* side-by-side */
      if (ubo.phi_range == 0) { /* 360 */
        sample_uv = vec2 (lat / 2.0f + gl_ViewIndex / 2.0f, lon);
      } else if (ubo.phi_range == 1) { /* 180 */
        sample_uv = vec2 (lat + gl_ViewIndex / 2.0f, lon);
      }
      break;
    case 2:
      /* top-bottom */
      sample_uv = vec2 (lat, lon / 2.0f + gl_ViewIndex / 2.0f);
      break;
    default:
      sample_uv = vec2 (lat, lon);
  };

  vec4 image_sample;
  if (ubo.phi_range == 0) { /* 360 */
    image_sample = texture (image, sample_uv);
  } else if (ubo.phi_range == 1) { /* 180 */
    if (lat > 0.0f && lat < 0.5f) {
      image_sample = texture (image, sample_uv);
    } else {
      image_sample = vec4 (0, 0, 0, 1);
    }
  }

#ifdef DEBUG
  out_color += image_sample / 2.0;
#else
  out_color = image_sample;
#endif

  if (ubo.gamma > 0 && ubo.gamma != 1.0f)
    out_color.rgb = pow (out_color.rgb, vec3 (1.0f / ubo.gamma));
}
